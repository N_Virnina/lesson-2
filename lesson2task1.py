import os.path
import numpy as np


def get_denominator(filename):
    denominator = 1
    flag = False
    if os.path.isfile(filename) == False:
        print(f"There is no file {filename}")
    else:
        f = open(filename, "r")
        d = f.read()
        f.close()
        try:
            d_int = int(d)
            if d_int != 0:
                denominator = d_int
                flag = True
            else:
                print("Given denominator is 0")
        except ValueError:
            print("The thing given in your file is not a natural number")
    return denominator, flag


def get_list_of_numbers(denominator):
    list_of_numbers = []
    if denominator <= 100:
        i = denominator
        while i <= 100:
            list_of_numbers.append(i)
            i += denominator
    return list_of_numbers


def get_sum(list_of_numbers):
    summa = np.sum(list_of_numbers)
    return summa


def write_result(number):
    f = open("result.txt", "w")
    f.write(str(number))
    f.close()


den, flag = get_denominator("denom.txt")
if flag == True:
    numbers = get_list_of_numbers(den)
    result = get_sum(numbers)
    write_result(result)
else:
    print("Try again with correct input")